export default class FetchData {
	constructor() {
		this.url = '/api/videos.json';
	}

	async getDataByUUID() {
		const url = this.url;
		try {
			const json = await fetch(url);
			return await json.json();
		} catch(err) {
			throw err;
		}
	}
}
