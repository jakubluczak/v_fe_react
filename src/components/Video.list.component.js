import React from 'react';

import VideoComponent from './video.component';
import TagsComponent from './tags.component';
import Form from './form.component';

export default class Main extends React.Component {
	render() {
		return (
			<div className="container">

				<div>
					<Form
						setText={this.props.setText}
						setTag={this.props.setTag}
					/>
				</div>

				<div className="video-list-container">
					<TagsComponent
						tags={this.props.tags}
						setTag={this.props.setTag}
						filters={this.props.filters}
					/>
					<div className="vidyard">
						<div className={"vidyard__videos-container"}>
							{this.props.videos.map(video => <VideoComponent key={Math.random()*Math.random()} video={video} />)}
						</div>
					</div>
				</div>
			</div>
		)
	}
}
