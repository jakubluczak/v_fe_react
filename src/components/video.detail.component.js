import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"

import React from 'react';
import Loader from 'react-loader-spinner'
import {Link} from 'react-router-dom';
import * as _ from 'lodash';
import vidyardEmbed from '@vidyard/embed-code';

import RelatedVideoComponent from './related-video.component';


export default class VideoComponent extends React.Component {
	constructor() {
		super();

		this.videoId = null;

		this.state = {
			isLoaderVisible: true,
			chapters: [{
				name: 'Chapter1',
				position: 5,
			}, {
				name: 'Chapter2',
				position: 15,
			}],
		};
	}

	get video() {
		return this.props.videos.find(el => el.uuid === this.props.videoId) || {};
	}

	componentDidMount() {
		// TODO - after getting back to list and then getting back to details didmount happens, but didupdate not
		if (!this.videoId) {
			this.componentDidUpdate()
		}

	}

	componentDidUpdate() {
		if ((this.state.isLoaderVisible === false && this.videoId !== this.props.videoId) || !this.videoId) {
			VideoComponent.destroyVideos();

			this.videoId = this.props.videoId;

			vidyardEmbed.api.renderPlayer({
				uuid: this.props.videoId,
				container: document.querySelector('.vidyard-video__container'),
				// type: 'lightbox',
				type: 'inline',
				aspect: 'landcape',
				vydata: encodeURIComponent(JSON.stringify({location: 'here', more: 'yes'})),
			}).then(() => {
				this.setState(() => ({
					isLoaderVisible: false,
				}));

				vidyardEmbed.api.progressEvents((result) => {
					VideoComponent.triggerAnalytics(result)
				}, [1, 25, 50, 75, 95]);

			});
		}
	}

	static triggerAnalytics(result) {
		try {
			console.log('analytics-trigger-here', {...result});
		} catch(e) {

		}
	}

	componentWillUnmount() {
		this.videoId = null;
		this.setState(() => ({
			isLoaderVisible: true
		}));
		VideoComponent.destroyVideos();
	}

	static destroyVideos() {
		// TODO - vidyard's destroyPlayer is not always working (sometimes it's not removing player from page, only from object), override manualy
		const container = document.querySelector('.vidyard-video__container');
		if (container) {
			while(container.firstChild) {
				container.removeChild(container.firstChild);
			}
		}
	}

	onClick(chapterElement) {
		vidyardEmbed.api.addReadyListener((data, player) => {
			player.seek(chapterElement.position);
			player.play();
		});
	}

	getRelatedVideos() {
		const {tags_attributes} = this.video;

		return this.props.videos.filter(video => {
			const videoTagIds = video.tags_attributes.map(element => element.id);
			const selectedVideoTagsIds = tags_attributes.map(element => element.id);
			const merged = _.uniq([...videoTagIds, ...selectedVideoTagsIds]);

			return merged.length !== videoTagIds.length + selectedVideoTagsIds.length && video.uuid !== this.props.videoId;
		});
	}

	render() {
		const video = this.video || {};
		const relatedVideos = this.getRelatedVideos() || [];

		return (
			<div className="vidyard-video">
				{this.state.isLoaderVisible && <div className="video__overlay">
					<Loader visible={this.state.isLoaderVisible} type="Hearts" color="blue" height={150} width={150}/>
				</div>}
				<div className="video-title">
					{
						video.name && (
							<div className="video__name">
								<h2>{video.name}</h2>
							</div>
						)
					}
					<Link className="back-link" to='/'>Back</Link>
				</div>
				<div className="vidyard-video__container"/>
				<div className="vidyard__chapters">
					{
						this.state.chapters.length ?

							this.state.chapters.map(chapter => (
									<div
										key={Math.random() * Math.random()}
										className="chapters__element"
									>
										<button
											type="button"
											className="chapters__button"
											onClick={this.onClick.bind(this, chapter)}
										>
											{chapter.name}
										</button>
									</div>
								)
							) : null
					}
				</div>

				<div className="vidyard-video__details">
					{
						video.description && (
							<div className="video__description">
								<h3>Description</h3>
								<p>{video.description}</p>
							</div>
						)
					}

				</div>

				{
					relatedVideos.length > 0 &&
					<div className="videos__related">
						<h3>Related Videos</h3>
						{
							relatedVideos.map(element => (
								<RelatedVideoComponent key={element.uuid} video={element}/>
							))
						}
					</div>
				}
			</div>
		)
	}

}
