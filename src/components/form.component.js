import React from 'react';

export default class Form extends React.Component {
	constructor() {
		super();
		this.onSubmit = this.onSubmit.bind(this);
		this.onChange = this.onChange.bind(this);
		this.onReset = this.onReset.bind(this);

		this.state = {
			formValue: ''
		}
	}
	render() {
		return (
			<div className="form-text-vide-container">
				<div className="reset-button__container">
					<button onClick={this.onReset} type="button">Reset</button>
				</div>
				<div className="form-text-video">
					<form
						onSubmit={this.onSubmit}
					>
						<input
							name="text-filter"
							type="text"
							placeholder="Filter by name"
							value={this.state.formValue}
							onChange={this.onChange}
						/>
					</form>
				</div>
			</div>
		)
	}

	onSubmit(e) {
		e.preventDefault();
	}

	onChange(e = {}) {
		const {target = {}} = e;

		if (target.value && this.state.formValue !== target.value) {
			this.setState(() => {
				return {
					formValue: target.value,
				}
			})

		}

		this.props.setText(target.value);
	}

	onReset() {
		this.setState(() => {
			return {
				formValue: '',
			}
		});

		this.props.setText();
		this.props.setTag();

	}
}
