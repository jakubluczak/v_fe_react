import React from 'react';
import {connect} from 'react-redux';
import VideoComponent from './video.detail.component';

class Video extends React.Component {
	constructor() {
		super();
	}

	render() {
		return (
			<VideoComponent videos={this.props.videos} videoId={this.props.match.params.id}/>
		)
	}
}


const ConnectedVideo = connect((state) => {
	return {
		videos: state.videos,
	}
})(Video);

export default ConnectedVideo;
