import React from "react";

export default class TagsComponent extends React.Component {
	constructor() {
		super();
		this.onTagClick = this.onTagClick.bind(this);
	}

	render() {
		return (<div className="vidyard-tags">
			<ul className="tags-list">
				{this.props.tags.map(tag => (
					<li key={Math.random()*Math.random()}>
						<a
							onClick={this.onTagClick}
							data-tag-id={tag.id}
							className={parseInt(tag.id) === parseInt(this.props.filters.tag) ? 'active' : ''}
						>{tag.name}</a>
					</li>
				))}
			</ul>
		</div>)
	}

	onTagClick(e) {
		const tagId = e.target.dataset.tagId;
		this.props.setTag(tagId);
	}
}
