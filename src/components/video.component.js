import React from 'react';
import {Link} from 'react-router-dom';

export default class VideoComponent extends React.Component {

	render() {
		const videoObject = this.props.video;
		return (
			<div className="vidyard-component">
				<Link to={`/video/${videoObject.uuid}`}>
					<div className="vidyard-component__thumbnail">
						<img className="thumbnail__image" src={videoObject.thumbnail} alt={videoObject.name}/>
					</div>
					<div className="vidyard-component__video-details">
						<div className="video-details__details">
							<h3 className="video-details__title">{videoObject.name || ''}</h3>
							<p className="video-details__description">{this._getVideoDescription(videoObject.description || '')}</p>
							<p className="video-details__length">{this._getVideoTime(videoObject)}</p>
						</div>
						<div className="video-details__tags">
							{videoObject.tags_attributes.map(tag => (
								<p key={Math.random() + tag.name} className="tags__tag"
								   data-tag-id={tag.id}>{tag.name}</p>
							))}
						</div>
					</div>
				</Link>
			</div>
		)
	}

	_getVideoTime(videoObject = {}) {
		const {seconds} = videoObject
		const timeInSeconds = parseInt(seconds) || 0;
		const sec = timeInSeconds % 60;
		const minutes = Math.floor(timeInSeconds / 60);

		return `${minutes}:${sec < 10 ? `0${sec}` : sec}`
	}

	_getVideoDescription(description) {

		const desc = description.substr(0, 100);

		if (description.length > 100) {
			return `${desc} (...)`;
		} else {
			return desc;
		}
	}
}
