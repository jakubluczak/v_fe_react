import { connect } from 'react-redux'
import { addVideos, setText, setTag, sortBy } from '../actions';
import VideoPage from './Video.list.component'


const getVisibleVideos = ({videos = [], filters = {}}) => {
	const {text = '', tag = null, sortBy = 'name'} = filters;
	return videos.filter(el => {
		const allowText =  el.name.toLowerCase().includes(text.toLowerCase());

		const allowTag = !tag || el.tags_attributes.find(elTag => {

			return parseInt(elTag.id) === parseInt(tag)
		});


		return allowText && allowTag;
	});
};

const mapStateToProps = (state) => ({
	videos: getVisibleVideos(state),
	tags: state.tags,
	filters: state.filters,
});

const mapDispatchToProps = dispatch => ({
	addVideo: list => dispatch(addVideos(list)),
	setText: text => dispatch(setText(text)),
	setTag: tag => dispatch(setTag(tag)),
	sortBy: el => dispatch(sortBy(el)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(VideoPage)
