import React from 'react';
import { Link } from 'react-router-dom';

export default class RelatedVideoComponent extends React.Component {
	render() {
		const video = this.props.video;

		return(
			<div className="related-video__container">
				<div className="related__image">
					<img src={video.thumbnail} alt={video.name} />
				</div>
				<div className="related__details">
					<Link to={`/video/${video.uuid}`}>
						<h4 className="related__name">{video.name}</h4>
					</Link>
					<p className="related__description">{video.description}</p>
				</div>
			</div>
		)
	}
}
