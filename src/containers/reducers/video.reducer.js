

const videoReducer = (state = [], action) => {
	switch(action.type) {
		case('ADD_LIST_OF_VIDEOS'): {
			return [...state, ...action.videosList];
		}
		default: {
			return state;
		}
	}
};

export default videoReducer;


