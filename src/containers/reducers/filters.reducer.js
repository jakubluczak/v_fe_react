const defaultState = {
	text: '',
	tag: null,
	sortBy: 'name',
};

const filterReducer = (state = defaultState, action) => {
	switch(action.type) {
		case('SET_TEXT'): {
			return {...state, text: action.text};
		}
		case('SET_TAG'): {
			return {...state, tag: action.tag};
		}
		case('SORT_BY'): {
			return {...state, sortBy: action.sortBy}
		}
		default: {
			return state;
		}
	}
};

export default filterReducer;


