const defaultState = [];

const tagsReducer = (state = defaultState, action) => {
	switch(action.type) {
		case('ADD_TAGS'): {
			return [...state, ...action.tags];
		}
		default: {
			return state;
		}
	}
};

export default tagsReducer;
