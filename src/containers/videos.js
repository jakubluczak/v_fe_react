import { combineReducers } from 'redux';

import videoReducer from './reducers/video.reducer';
import filterReducer from './reducers/filters.reducer';
import tagsReducer from "./reducers/tags.reducer";

const reducer = combineReducers({
	videos: videoReducer,
	filters: filterReducer,
	tags: tagsReducer,
});

export default reducer;


