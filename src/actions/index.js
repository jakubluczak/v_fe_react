export const addVideos = (videos = []) => ({
	type: 'ADD_LIST_OF_VIDEOS',
	videosList: videos
});

export const setText = (text = '') => ({
	type: 'SET_TEXT',
	text
});

export const setTag = (tag = null) => ({
	type: 'SET_TAG',
	tag
});

export const sortBy = (sortBy = 'name') => {
	if (sortBy === 'date') {
		return {
			type: 'SET_TEXT',
			sortBy: 'date',

		}
	}
	return {
		type: 'SET_TEXT',
		sortBy: 'name',

	}
};
