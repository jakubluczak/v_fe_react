import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { HashRouter, Route, Switch } from 'react-router-dom';

import './App.scss';
import reducer from './containers/videos.js';

import Main from './components/Main.page';
import VideoDetails from './components/video.detail.component';

import FetchData from './services/fetchData';
import ConnectedVideo from "./components/video.details.page";
const fetchData = new FetchData();

const store = createStore(reducer);


// TODO - move to redux-promise middleware
fetchData.getDataByUUID().then(res => {
    store.dispatch({type: 'ADD_LIST_OF_VIDEOS', videosList: res.videos});
    store.dispatch({
        type: 'ADD_TAGS',
        tags: res.tags
    })
});


function App() {
  return (
      <Provider store={store}>
          <HashRouter
              basename='/'
          >
              <Switch>
            <Route path='/' component={Main} exact/>
            <Route path='/video/:id' component={ConnectedVideo}/>
              </Switch>
          </HashRouter>
      </Provider>
  );
}

export default App;
